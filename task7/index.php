<?php
$currentDate = new DateTime();
$today = new DateTime();
$todayDateInfo = getdate($today->getTimestamp());
if (isset($_GET['year'])) {
    $currentDate->setDate($_GET['year'], $_GET['month'], 3);
}
$dateInfo = getdate($currentDate->getTimestamp());
$currentMonth = $dateInfo['mon'];
$currentYear = $dateInfo['year'];
$prevMonth = ($currentMonth > 1) ? ($currentMonth - 1) : 12;
$prevYear = ($currentMonth > 1) ? $currentYear : $currentYear - 1;
$nextMonth = ($currentMonth < 12) ? ($currentMonth + 1) : 1;
$nextYear = ($currentMonth < 12) ? $currentYear : $currentYear + 1;
$calDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
$weekDayFirstDay = $dateInfo['wday'];
$monthString = $dateInfo['month'];
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="calendar.css">
</head>

<body>

<div class="custom-calendar-wrap">
    <div class="custom-inner">
        <div class="custom-header clearfix">
            <nav>
                <a href="?month=<?php echo $prevMonth ?>&year=<?php echo $prevYear ?>"
                   class="custom-btn custom-prev"></a>
                <a href="?month=<?php echo $nextMonth ?>&year=<?php echo $nextYear ?>"
                   class="custom-btn custom-next"></a>
            </nav>
            <h2 id="custom-month" class="custom-month"><?php echo $monthString ?></h2>
            <h3 id="custom-year" class="custom-year"><?php echo $currentYear ?></h3>
        </div>
        <div id="calendar" class="fc-calendar-container">
            <div class="fc-calendar fc-five-rows">
                <div class="fc-head">
                    <div>Sun</div>
                    <div>Mon</div>
                    <div>Tue</div>
                    <div>Wed</div>
                    <div>Thu</div>
                    <div>Fri</div>
                    <div>Sat</div>
                </div>
                <div class="fc-body">
                    <?php
                    $count = 0;
                    for ($x = 0; $x < 5; $x++) {
                        ?>
                        <div class="fc-row">
                            <?php
                            for ($i = 0; $i < 7; $i++) {
                                $dayInMonth = $count - $weekDayFirstDay;
                                $isToday = false;
                                if($dayInMonth == $todayDateInfo["mday"] && $todayDateInfo["mon"] ==$currentMonth && $todayDateInfo["year"] === $currentYear) {
                                    $isToday = true;
                                }
                                    ?>
                                <div class="<?php echo ($isToday ? "fc-today" : "" ) ?>"><span class="fc-date "><?php
                                        if($count > $weekDayFirstDay ) {
                                            if($dayInMonth <= $calDaysInMonth) {
                                                echo $dayInMonth ;
                                            }
                                        }
                                        ?> </span></div>
                                <?php
                                $count++;
                            }
                            ?>
                        </div>

                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>


</body>

</html>